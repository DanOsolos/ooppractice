public class Collage  {
    private String university;
    private String collageName;
    private Country country;

    public Collage () {}

    public Collage (String university, String collageName, Country country){
        this.university = university;
        this.collageName = collageName;
        this.country = country;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getCollageName() {
        return collageName;
    }

    public void setCollageName(String collageName) {
        this.collageName = collageName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Collage{" +
                "university='" + university + '\'' +
                ", collageName='" + collageName + '\'' +
                ", country=" + country +
                '}';
    }
}
