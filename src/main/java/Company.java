public class Company {
    private String companyName;
    private Country country;

    public Company() {}

    public Company(String companyName, Country country){
        this.companyName = companyName;
        this.country = country;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Company{" +
                "companyName='" + companyName + '\'' +
                ", country=" + country +
                '}';
    }
}
