public class Country {
    private String countryName;
    private String countryCode;
    private boolean isEu;

    public Country () {}

    public Country(String countryName, String countryCode, boolean isEu){
        this.countryName = countryName;
        this.countryCode = countryCode;
        this.isEu = isEu;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public boolean isEu() {
        return isEu;
    }

    public void setEu(boolean eu) {
        isEu = eu;
    }

    @Override
    public String toString() {
        return "Country{" +
                "countryName='" + countryName + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", isEu=" + isEu +
                '}';
    }
}
