public class Employee extends Person{
    private Company company;
    private int salary;

    public Employee () {}

    public Employee (Company company, int salary, String name, String CNP, int age, Address address, int numberOfWorkedHours){
        super(age, name, CNP, numberOfWorkedHours, address);
        this.salary = salary;
        this.company = company;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "company=" + company +
                ", salary=" + salary +
                '}';
    }
}
