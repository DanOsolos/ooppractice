public class Main {
    public static void main(String[] args) {
        Country studentCountry = new Country("Romania", "RO", true);
        Country employeeCountry = new Country("Germany", "GE", true);
        Country unemployedCountry = new Country("Spain", "E", true);


        Country collageCountry = new Country("Canada", "CA", false);
        Country companyCountry = new Country("United States of America", "USA", false);

        Address studentAddress = new Address(studentCountry, "Iasi", "123456");
        Address employeeAddress = new Address(employeeCountry, "Bacau", "162376");
        Address unemployedAddress = new Address(unemployedCountry, "Roman", "456762");

        Company company = new Company("Google", companyCountry);
        Collage collage = new Collage("Univ de Medicina si Farmacie", "Facultatea de Bioinginerie Medicala", collageCountry);

        Person personStudent = new Person(29, "Dan", "1960122045357", 0, studentAddress);
        Person personEmployee = new Person(55, "Ion", "1960122045357", 100, employeeAddress);
        Person personUnemployed = new Person(18, "Stefan", "1960122045357", 0, unemployedAddress);

        Student student = new Student(collage, 3, personStudent.getName(), personStudent.getCNP(), personStudent.getAge(),
                personStudent.getAddress(), personStudent.getNumberOfWorkedHours());

        Employee employee = new Employee(company, 300000, personEmployee.getName(), personEmployee.getCNP(), personEmployee.getAge(),
                personEmployee.getAddress(), personEmployee.getNumberOfWorkedHours());

        Unemployed unemployed = new Unemployed(true, "Software engineer", personUnemployed.getName(),
                personUnemployed.getCNP(), personUnemployed.getAge(), personUnemployed.getAddress(), personUnemployed.getNumberOfWorkedHours());

        System.out.println("Student data\nThe student: " + student.getName() + " aged " + student.getAge()
                + " with the CNP " + student.getCNP() + " from the country " + studentCountry.getCountryName() + " with the country code "
                + studentCountry.getCountryCode() + " which is a EU country '" + studentCountry.isEu()
                + "',\nfrom the city " + studentAddress.getCity() + " and the postal code " + studentAddress.getPostalCode()
                + " is a student at:\n" + collage.getUniversity() + " at " + collage.getCollageName() + " from " + collageCountry.getCountryName());

        System.out.println("\nEmployee data\nThe employee " + employee.getName() + " aged " + employee.getAge()
                + " with the CNP " + employee.getCNP() + " from the country " + employeeCountry.getCountryName() + " with the country code "
                + employeeCountry.getCountryCode() + " which is a EU country '" + employeeCountry.isEu()
                + "',\nfrom the city " + employeeAddress.getCity() + " and the postal code " + employeeAddress.getPostalCode()
                + " is working at " + company.getCompanyName() + " in " + companyCountry.getCountryName()
                + " with " + employee.getNumberOfWorkedHours() + " worked hours and has a salary of " + employee.getSalary() + "$");

        System.out.println("\nUnemployed data\nThe unemployed " + unemployed.getName() + " aged " + unemployed.getAge()
                + " with the CNP " + unemployed.getCNP() + " from the country " + unemployedCountry.getCountryName() + " with the country code "
                + unemployedCountry.getCountryCode() + " which is a EU country '" + unemployedCountry.isEu()
                + "',\nfrom the city " + unemployedAddress.getCity() + " and the postal code " + unemployedAddress.getPostalCode()
                + " is currently working '" + unemployed.getNumberOfWorkedHoursInAWeek(unemployed.getNumberOfWorkedHours())
                + "' and his basic profession is " + unemployed.getBasicProfession());

    }
}
