public class Person {
    private int age;
    private String name;
    private String CNP;
    private Address address;
    private int numberOfWorkedHours;

    public Person () {}

    public Person (int age, String name, String CNP,  int numberOfWorkedHours, Address address){
        this.age = age;
        this.address = address;
        this.name = name;
        this.CNP = CNP;
        this.numberOfWorkedHours = numberOfWorkedHours;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public boolean getNumberOfWorkedHoursInAWeek (int numberOfWorkedHours) {
        return numberOfWorkedHours > 0;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getNumberOfWorkedHours() {
        return numberOfWorkedHours;
    }

    public void setNumberOfWorkedHours(int numberOfWorkedHours) {
        this.numberOfWorkedHours = numberOfWorkedHours;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", CNP='" + CNP + '\'' +
                ", address=" + address +
                ", numberOfWorkedHours=" + numberOfWorkedHours +
                '}';
    }
}
