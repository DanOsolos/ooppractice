public class Student extends Person {
    private Collage collage;
    private int studyYear;

    public Student () {}

    public Student (Collage collage, int studyYear, String name, String CNP, int age, Address address, int numberOfWorkedHours){
        super(age, name, CNP, numberOfWorkedHours, address);
        this.collage = collage;
        this.studyYear = studyYear;
    }

    public Collage getCollage() {
        return collage;
    }

    public void setCollage(Collage collage) {
        this.collage = collage;
    }

    public int getStudyYear() {
        return studyYear;
    }

    public void setStudyYear(int studyYear) {
        this.studyYear = studyYear;
    }

    @Override
    public String toString() {
        return "Student{" +
                "collage=" + collage +
                ", studyYear=" + studyYear +
                '}';
    }
}
