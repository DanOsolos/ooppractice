public class Unemployed extends Person {
    private boolean freeTime;
    private String basicProfession;

    public Unemployed () {}
    public Unemployed (boolean freeTime, String basicProfession, String name, String CNP, int age, Address address, int numberOfWorkedHours){
        super(age, name, CNP, numberOfWorkedHours, address);
        this.freeTime = freeTime;
        this.basicProfession = basicProfession;
    }

    public boolean isFreeTime() {
        return freeTime;
    }

    public void setFreeTime(boolean freeTime) {
        this.freeTime = freeTime;
    }

    public String getBasicProfession() {
        return basicProfession;
    }

    public void setBasicProfession(String basicProfession) {
        this.basicProfession = basicProfession;
    }

    @Override
    public String toString() {
        return "Unemployed{" +
                "freeTime=" + freeTime +
                ", basicProfession='" + basicProfession + '\'' +
                '}';
    }
}
